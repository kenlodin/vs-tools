# Outils

## Diffusion de live
Outil permettant de transcoder un flux sdp en un flux accepté par le serveur

	./livediff <sdp file>

## Ajout de film
Permet d'ajouter tous les films d'un dossier http directement dans la base de données du serveur

	./addvideo.py <option...> <host>
	  --help|-h	Draw this
	  --folder|-f	Set the base folder		
