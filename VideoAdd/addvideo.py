#!/usr/bin/env python

import getopt, sys, os

def sendFilm(filmPath):
  request = "http://fake?name=%s" % sys.argv[len(sys.argv)-1] + filmPath
  print request

def checkFile(filePath):
  if not os.path.splitext(filePath)[1] == ".avi":
    return
  print "Add " + os.path.basename(filePath) + "?[Yes][No][I am stupid]:",
  line = sys.stdin.readline().rstrip()
  if not line in ("Y", "y", "o", "O", "Yes", "yes", "oui", "Oui"):
    return
  sendFilm(filePath)
  

def searchFilm(folderPath):
  files = os.listdir(folderPath);
  for f in files:
    f = folderPath + "/" + f
    if os.path.isdir(f) == True:
      searchFilm(f) 
    else:
      checkFile(f)

# main

def usage():
  print "./addvideo.py [OPTION] HOST"
  print "\t--help|-h\t\tDraw this"
  print "\t--folder|-f\t\tSet the base folder"

def main():
  baseFolder = "."
  
  if len(sys.argv) < 2:
    usage()
    sys.exit(2)

  try:
    opts, args = getopt.getopt(sys.argv[1:], "hf:", ["help", "folder"])
  except getopt.GetoptError, err:
    print str(err)
    usage()
    sys.exit(2)

  for opt, arg in opts:
    if opt in ("-f", "--folder"):
      print "Set base folder to: " + arg
      baseFolder = arg
    if opt in ("-h", "--help"):
      usage()
      sys.exit(0)

  searchFilm(baseFolder)

if __name__ == "__main__":
  main()
