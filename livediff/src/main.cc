//C++ headers
#include <iostream>

//C headers
#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<fcntl.h>
#include <signal.h>

pid_t pid = 0;

void terminate (int param)
{
  if (pid != 0)
    kill(pid, SIGKILL);
  printf ("\nTerminating program...\n");
  exit(1);
}


//http://sites.google.com/site/williamedwardscoder/popen3
int popen3(int fd[3], const char **const cmd)
{
    int i, e;
    int p[3][2];

    // set all the FDs to invalid
    for (i = 0; i < 3; i++)
	p[i][0] = p[i][1] = -1;

    // create the pipes
    for (int i = 0; i < 3; i++)
	if (pipe(p[i]))
	    goto error;

    // and fork
    pid = fork();

    signal (SIGTERM, terminate);
    signal (SIGINT,  terminate);

    if (-1 == pid)
	goto error;

    // in the parent?
    if (pid) {
	// parent
	fd[STDIN_FILENO] = p[STDIN_FILENO][1];
	close(p[STDIN_FILENO][0]);
	fd[STDOUT_FILENO] = p[STDOUT_FILENO][0];
	close(p[STDOUT_FILENO][1]);
	fd[STDERR_FILENO] = p[STDERR_FILENO][0];
	close(p[STDERR_FILENO][1]);
	// success
	return 0;
    } else {
	// child
	dup2(p[STDIN_FILENO][0], STDIN_FILENO);
	close(p[STDIN_FILENO][1]);
	dup2(p[STDOUT_FILENO][1], STDOUT_FILENO);
	close(p[STDOUT_FILENO][0]);
	dup2(p[STDERR_FILENO][1], STDERR_FILENO);
	close(p[STDERR_FILENO][0]);
	// here we try and run it
	execvp(*cmd, const_cast < char *const *>(cmd));
	// if we are there, then we failed to launch our program
	perror("Could not launch");
	fprintf(stderr, " \"%s\"\n", *cmd);
	_exit(EXIT_FAILURE);
    }
  error:
    // preserve original error
    e = errno;
    for (i = 0; i < 3; i++) {
	close(p[i][0]);
	close(p[i][1]);
    }
    errno = e;
    return -1;
}

//http://www.kegel.com/dkftpbench/nonblocking.html
int setNonblocking(int fd)
{
    int flags;

    /* If they have O_NONBLOCK, use the Posix way to do it */
#if defined(O_NONBLOCK)
    /* Fixme: O_NONBLOCK is defined but broken on SunOS 4.1.x and AIX 3.2.5. */
    if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
        flags = 0;
    return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
    /* Otherwise, use the old way of doing it */
    flags = 1;
    return ioctl(fd, FIOBIO, &flags);
#endif
}

void transcode(const char *format, const char *sdp_file)
{
    int fd[3];
    int pid;

    const char *cmd[] = { "ffmpeg",
	"-i",
	sdp_file,
	"-vcodec",
	"copy",
	"-f",
	format,
	"-",
	NULL
    };

    std::cout << cmd[0] << " "
	<< cmd[1] << " "
	<< cmd[2] << " "
	<< cmd[3] << " "
	<< cmd[4] << " "
	<< cmd[5] << " "
	<< cmd[6] << " "
        << cmd[7] << std::endl;

    if ((pid = popen3(fd, cmd)) == -1) {
    	std::cout << "popen3 error" << std::endl;
    }

    setNonblocking(fd[1]);
    //setNonblocking(fd[2]); Not necessairy if use complet read chunk

    int   filefd = open("film.avi", O_WRONLY | O_CREAT, 0666);
    char  buf[512];
    int   count;
    while ((count = read(fd[2], buf, 512)) != 0)
    {
      //std::cout << "out:" << count << std::endl;
      write(STDERR_FILENO, buf, count);
      count = read(fd[1], buf, 512);
      //std::cout << "err:" << count << std::endl;
      write(filefd, buf, count);
    }
    std::cout << count << std::endl;
    close(filefd);
}
////
int main(int argc, char **argv)
{
    if (argc == 1)
      exit(0);
    std::cout << "Live Diffusion Avi by kokarez" << std::endl;
    transcode("avi", argv[1]);
    terminate(0);
    return 0;
}
